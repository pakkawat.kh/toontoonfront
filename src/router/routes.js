const routes = [
  // {
  //   path: "/",
  //   component: () => import("layouts/MainLayout.vue"),
  //   children: [{ path: "", component: () => import("pages/Index.vue") }],
  // },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/",
    component: () => import("pages/main.vue"),
    name: "main",
  },
  {
    path: "/category/:cat?",
    component: () => import("pages/category.vue"),
  },
  {
    path: "/search/:searchtxt",
    component: () => import("pages/search.vue"),
  },
  {
    path: "/chapter/:bookID",
    component: () => import("pages/chapter.vue"),
  },

  {
    path: "/new",
    component: () => import("pages/new.vue"),
  },
  {
    path: "/reload/:page",
    component: () => import("pages/reload.vue"),
  },
  {
    path: "*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
